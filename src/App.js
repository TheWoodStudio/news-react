import React, { Component } from 'react';
import Header from './components/Header';
import SearchForm from './components/SearchForm';
import News from './components/News';
import Container from 'muicss/lib/react/container';

class App extends Component {

  state = {
    news: []
  }

  componentDidMount() {
    this.getNews();
  }

  //Arrow function with default parameter
  getNews = (category = 'general', country = 'ar') => {
    let url = `https://newsapi.org/v2/top-headlines?country=${country}&category=${category}&apiKey=f89b59de0913450cada46bb369209934`;
    
    fetch(url)
      .then(response => {
        return response.json();
      })
      .then(news => {
        this.setState({
          news: news.articles
        });
      })
  }

  render() {
    return (
      <div className="App">
        <Header
          title="World News"
        />

        <Container>
          <SearchForm 
            getNews={this.getNews}
          />
          <News 
            news={this.state.news}
          />
        </Container>
      </div>
    );
  }
}

export default App;
