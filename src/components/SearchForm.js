import React, { Component } from 'react';
import Form from 'muicss/lib/react/form';
// import Input from 'muicss/lib/react/input';
import Option from 'muicss/lib/react/option';
import Select from 'muicss/lib/react/select';
import Button from 'muicss/lib/react/button';
import PropTypes from 'prop-types';

class searchForm extends Component {
    switchCategory = (e) => {
        e.preventDefault();
        
        //Get ref
        const categoryRef = this.categoryRef.controlEl.value;
        const countryRef = this.countryRef.controlEl.value;

        //Send by props
        this.props.getNews(categoryRef, countryRef);
    }

    render() { 
        return ( 
            <Form onSubmit={this.switchCategory}>
                <Select label="Category" defaultValue="general" ref={el => { this.categoryRef = el; }}>
                    <Option value="business" label="Business" />
                    <Option value="entertainment" label="Entertainment" />
                    <Option value="general" label="General" />
                    <Option value="health" label="Health" />
                    <Option value="science" label="Science" />
                    <Option value="sports" label="Sports" />
                    <Option value="technology" label="Technology" />
                </Select>
                <Select label="Country" defaultValue="ar" ref={el => { this.countryRef = el; }}>
                    <Option value="ar" label="Argentina" />
                    <Option value="at" label="Austria" />
                    <Option value="au" label="Australia" />
                    <Option value="be" label="Belgium" />
                    <Option value="bg" label="Bulgaria" />
                    <Option value="br" label="Brazil" />
                    <Option value="ca" label="Canada" />
                    <Option value="cn" label="China" />
                    <Option value="co" label="Colombia" />
                    <Option value="cu" label="Cuba" />
                    <Option value="cz" label="Czech Republic" />
                    <Option value="eg" label="Egypt" />
                    <Option value="fr" label="France" />
                    <Option value="gr" label="Greece" />
                    <Option value="hk" label="Hong Kong" />
                    <Option value="hu" label="Hungary" />
                    <Option value="id" label="Indonesia" />
                    <Option value="ie" label="Ireland" />
                    <Option value="il" label="Israel" />
                    <Option value="in" label="India" />
                    <Option value="it" label="Italy" />
                    <Option value="jp" label="Japan" />
                    <Option value="kr" label="Korea" />
                    <Option value="de" label="Germany" />
                    <Option value="ch" label="Switzerland" />
                    <Option value="ae" label="United Arab Emirates" />
                    <Option value="gb" label="United Kingdom" />
                    
                </Select>
                <Button color="primary" type="submit">Filter</Button>
            </Form>
         );
    }
}
 
searchForm.propTypes = {
    getNews: PropTypes.func.isRequired
}
export default searchForm;