import React, { Component } from 'react';
import SingleArticle from './SingleArticle';
import Row from 'muicss/lib/react/row';
import { TransitionGroup, CSSTransition } from 'react-transition-group';
import PropTypes from 'prop-types';

class News extends Component {
    renderArticle(singleArticle) {
      return (
        <CSSTransition key={singleArticle.url} classNames="fade" timeout={500}>
          <SingleArticle singleArticle={singleArticle} />
        </CSSTransition>
      );
    }
  
    renderRow(articles) {
      return (
        <Row>
          <TransitionGroup>{articles}</TransitionGroup>
        </Row>
      );
    }
  
    renderRows() {
      const { news } = this.props;
      let articles = [];
      let rows = [];
  
      for (let i = 0; i < news.length; i++) {
        const singleArticle = news[i];
        // Deberías acumular artículos para eventualmente agregarlos a la fila
        articles.push(this.renderArticle(singleArticle));
        // Se realiza % 3, ya que necesitas un comportamineto distinto cada tres
        // elementos, específicamente en la tercera iteración (i = 2, 5, 8, etc...)
        // es cuando deberías crear la fila y agregar los artículos acumulados
        if (i % 3 === 2) {
          rows.push(this.renderRow(articles));
          // Y luego vaciar el arreglo de artículos, ya que ya fueron agregados a
          // la nueva fila
          articles = [];
        }
      }
      // En caso de que haya quedado algún artículo sin agregar al arreglo de filas,
      // se agregará en esta última etapa
      if (articles.length > 0) {
        rows.push(this.renderRow(articles));
      }
      return rows;
    }
  
    render() {
      return <div>{this.renderRows()}</div>;
    }
  }

News.propTypes = {
    news: PropTypes.array.isRequired
}
 
export default News;