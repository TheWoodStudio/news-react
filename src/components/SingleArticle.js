import React from 'react';
import Panel from 'muicss/lib/react/panel';
import Col from 'muicss/lib/react/col';
import PropTypes from 'prop-types';

const SingleArticle = (props) => {
    const { urlToImage, url, description, title } = props.singleArticle;
    
    const image = (urlToImage) ? <img src={urlToImage} alt={title} /> : '';
    return (
        <Col md="4">
            <Panel>
                {image}
                <h3>{title}</h3>
                <p>{description}</p>
                <a href={url}>+</a>
            </Panel>
        </Col>

      );
};

SingleArticle.propTypes = {
    singleArticle: PropTypes.shape({
        urlToImage: PropTypes.string,
        url: PropTypes.string,
        description: PropTypes.string,
        title: PropTypes.string
    })
}

export default SingleArticle;