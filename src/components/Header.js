import React from 'react';
import Container from 'muicss/lib/react/container';
import PropTypes from 'prop-types';

const Header = (props) => {
    return ( 
        <header>
            <Container>
                <h1>{props.title}</h1>
            </Container>
        </header>
     );
}

Header.propTypes = {
    title: PropTypes.string.isRequired
}
 
export default Header;